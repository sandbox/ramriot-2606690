<?php

define('ANON_ENTITY_URL', 'a-auth');
//debugging message function
/*
function dsmp()
{
    drupal_set_message("<pre>" . print_r(func_get_args(), true) . "</pre>");
}
// */
/**
 * NOTES:
 * *
 * * Long term session coockie is to stand in for authentication on pseudononymous
 * * accounts. When a cookie is present and there is an associated account the user
 * * is authenticated to that account. When there is no cookie present none is added
 * * unless navigating to a page where an enabled entity is to be created that requires
 * * ownership. When such an entity is created, the before_save is hooked and a
 * * pseudononymous account is created, that account is made the logged in account
 * * the cookie sid is associated with that account and the entity ownership is also.
 */

 /**
 * Implement hook_menu().
 */
function anon_entity_menu() {
  $items['admin/config/development/'.ANON_ENTITY_URL] = array(
    'title'				=> t('Anonymous Entity Configuration'),
    'description'		=> t('Configure Anonymous Entity behavior.'),
    'access arguments'	=> array('administer site configuration'),
    'page callback'		=> 'drupal_get_form',
    'page arguments'	=> array('anon_entity_settings_form'),
    'file'				=> 'anon_entity.admin.inc',
  );
  $items['user/'.ANON_ENTITY_URL] = array(
    'title'				=> 'Anonymous Account',
	'page callback'		=> 'drupal_get_form',
    'page arguments'	=> array('anon_entity_login_form'),
    'access callback'	=> 'user_is_anonymous',
    'type'				=> MENU_LOCAL_TASK,
    //'file'				=> 'user.pages.inc',
  );

    $items['user/%user/'.ANON_ENTITY_URL] = array(
    'title' => 'Anonymous Account',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('anon_entity_login_form'),
    'access callback' => 'user_is_logged_in',
    'type'				=> MENU_LOCAL_TASK,
  );

  return $items;
}

/**
 * Implement hook_cron().
 */
function anon_entity_cron() {
  // Fetch list of outdated sids.
  $query = db_select('anon_entity', 'sap');
  //GM join is meaningless now I think?
  //$query->leftJoin('sessions', 's', 'sap.sid = s.sid');
  $query->fields('sap', array('sid'));
  $query->condition('sap.timestamp', REQUEST_TIME - variable_get('anon_entity_cookie_expire_time', 2592000), '<');
  $outdated_sids = $query->execute()->fetchCol();

  if (!empty($outdated_sids)) {
    module_invoke_all('anon_entity_cleanup', $outdated_sids);
    db_query('DELETE FROM {anon_entity} WHERE sid IN (:sids)', array(':sids' => $outdated_sids));
  }
}

/**
 * Function for anonymous login trigger page
 * This page should display the following:-
 * Page should not be needed if user already logged in.
 * If authentication achieved => Success, notes and recovery code.
 * If authentication failed => Fail, determine cause, notes and field to insert recovery code.
 * This page assumes the cookie is available because the menu access callback
 * tests for this.
 */
function anon_entity_login_form($form, &$form_state)	{
	static $record = FALSE;
    anon_entity_disable_page_cache();
	//Get or construct session cookie and record
	$record = anon_entity_get_session();
	
	$form['record']	=	array(
		'#type'	=>	'value',
		'#value'	=>	$record,
	);
	
	if($record->uid == 0)//A new session had to be spawned
	{
		//Offer recovery field
		$form['recovery_id']	=	array(
			'#type'			    =>	'textfield',
			'#title'		    =>	t('Anonymous account recovery key'),
			'#description'	    =>	t('If you have an Anonymous account recovery key from a previous visit, place it here to be authenticated again.'),
		);
		$form['submit']	=	array(
		  '#type'	=>	'submit',
		  '#value'	=>	t('Recover'),
		);
	}
	elseif($record->uid)//Pseudononymous user exists
	{
		//authenticate user as needed
		if(user_is_anonymous())
		{
			$account = user_load($record->uid);
			if($account)
			{
				anon_entity_account_login($account);
				drupal_goto('/user/'.$record->uid.'/'.ANON_ENTITY_URL);
			}
		}
		global $user;
		$days = intval(variable_get('anon_entity_cookie_expire_time', 2592000) / 68400);
		$left = intval(($record->timestamp + variable_get('anon_entity_cookie_expire_time', 2592000) - time()) / 68400);
		$tokens = array(
		  '@name'	=>	$user->name,
		  '!profile_link'		=>	l('Profile', '/user/'.$user->uid.'/edit'),
		  '@days'				=>	$days,
		  '@left'				=>	$left,
		);
		$form['notes']	=	array(
			'#markup'	=>	t('Hello @name You have been re-authenticated to your anonymous account. You have @left days left to use this feature before you will need to upgrade your !profile_link to a full account', $tokens),
		);
		$form['recovery_id']	=	array(
			'#type'			=>	'textfield',
			'#title'		=>	t('Anonymous account recovery key'),
			'#description'	=>	t('Please securely save this recovery key. It will allow you to authenticate to your Anonymous account should the cookie not be available.', $tokents),
			'#default_value'	=>	$record->recovery_id,
		);
	}
	else
	{
		//Failure due to external issues
	}
	return $form;
}

function anon_entity_login_form_submit($form, &$form_state)	{
	global $user;
	$values = $form_state['values'];
	$old_record = $values['record'];
	$recovery_id = trim($values['recovery_id']);
	if($old_record->uid == 0 && $recovery_id)
	{
		//Get record from token
		$record = anon_entity_get_record($recovery_id, 'recovery_id');
		if(!$record) return;
		$account = user_load($record->uid);
		if(!$account) return;
		//replace client cookie
		anon_entity_set_cookie($record);
		//authenticate user
		anon_entity_account_login($account);
	}
	if(user_is_logged_in())
	{
		$form_state['redirect'] = '/user/'.$record->uid.ANON_ENTITY_URL;
	}
	else
	{
		$form_state['redirect'] = '/user/'.ANON_ENTITY_URL;
	}
	
}

/**
 * Disable the Drupal page cache.
 */
function anon_entity_disable_page_cache() {
  drupal_page_is_cacheable(FALSE);
}

/**
 * Return a Session API ID corresponding to the current session.
 * The session id is a created using the same method as Drupal core
 * as defined in includes/session.inc.
 * @param boolean create set this to FALSE if you don't want to create a session
 * cookie if it doesn't already exist.
 */
function anon_entity_get_session() {
	//Set record to static for repeated calls
	static $record = FALSE;
	//Trap multiple calls to function and return quickly after ensuring everything set up
	if ($record) {
		//ensure cookie is set, function call is singular
		anon_entity_set_cookie($record);
		//ensure record up to date, function call is singular
		//anon_entity_update_record_timestamp($record);
		return $record;
	}
	//Thus First call to function, check if we already have a session cookie
	if($session_id = anon_entity_get_cookie())
	{
		//fetch record matching session_id
		$record = anon_entity_get_record($session_id, 'session_id');
	}

	//If no record found above, ignore cookie value and reset with new cookie
	if(!$record) {
		$record = anon_entity_construct_record();
		//ensure cookie is set, function call is singular
		anon_entity_set_cookie($record);
	}
	return $record;
}

/**
 * function to get and return the anonymous session cookie if present
 */
function anon_entity_get_cookie()	{
    // For the cookie we use the same domain that Drupal's own session cookie uses.
    $cookie_domain = ini_get('session.cookie_domain');
	//Cookies available?
	if (anon_entity_available() && isset($_COOKIE['anon_entity_session']) && $_COOKIE['anon_entity_session']) {
		//get session_id from cookie
		return $_COOKIE['anon_entity_session'];
	}
	return FALSE;
}

/**
 * function to set anonymous authentication cookie ONCE per session
 */
function anon_entity_set_cookie($record)	{
	static $cookie_set = FALSE;	
	if($cookie_set) return;
    // For the cookie we use the same domain that Drupal's own session cookie uses.
    $cookie_domain = ini_get('session.cookie_domain');
	//Detect secure connection
	global $is_https;
	$secure = $is_https;
	//Set http only for cookie always
	$http_only = TRUE;
	// Set cookie.
	setcookie('anon_entity_session', $record->session_id, $record->timestamp + variable_get('anon_entity_cookie_expire_time', 2592000), '/', $cookie_domain, $secure, $http_only);
	$cookie_set = TRUE;
}

/**
 * Determine if cookies are enabled.
 */
function anon_entity_available() {
  return !empty($_COOKIE);
}

/**
 * Implements hook_entity_presave() to set up pseudonomimous ownership
 */
function anon_entity_entity_presave($entity, $type) {
	//if user already logged in do nothing
    if (user_is_logged_in()) return;
	//Construct tag
	list($id, $vid, $bundle) = entity_extract_ids($type, $entity);
	$tag = $type . ':' . $bundle;
	$types = array_filter(variable_get('anon_entity_entity_selection', array()));

    //Get array of active types
	if(in_array($tag, $types))
	{
		//get or generate sid
		$record = anon_entity_get_session();
		if(!$record) return;
		if(!$record->uid)//record exists but uid evaluates to false
		{
			//generate psudononymous account
			$account = anon_entity_construct_pseusdononymous($sid);
			$record->uid = $account->uid;
			drupal_write_record('anon_entity', $record, 'sid');
		}
		else
		{
			$account = user_load($record->uid);
		}
		//force authentication and login
		anon_entity_account_login($account);
		//set entity ownership
		$entity->uid = $account->uid;
	}
}

/**
 * Function to construct record
 */
function anon_entity_construct_record()
{
	$session_id = drupal_hash_base64(uniqid(mt_rand(), TRUE));
	$recovery_id = user_password(12);
	//Instantiate instance of record, but with no associated user
	$rec = new stdClass();
	$rec->timestamp = REQUEST_TIME;
	$rec->session_id = $session_id;
	$rec->recovery_id = $recovery_id;
	$rec->uid = 0;
	//insert new session record
	drupal_write_record('anon_entity', $rec);
	return $rec;
}

/**
 * Function to generate unique usernames
 */
function anon_entity_construct_name()	{
	$name = 'anon-'.user_password(5)."-".$sid;
	//uniqueness check
	if(user_load_by_name($name))
	{
		return anon_entity_construct_name();
	}
	return $name;
}
/**
 * Function to generate a pseudononymous account
 */
function anon_entity_construct_pseusdononymous($sid)	{
	//This will generate a random password, you could set your own here
	$password = user_password(8);
	$name = anon_entity_construct_name();
	//set up the user fields
	$fields = array(
		'name' => $name,
		'mail' => '',
		'pass' => $password,
		'status' => 1,
		//'init' => 'email address',
		'roles' => array(
		  DRUPAL_AUTHENTICATED_RID => 'authenticated user',
		),
	);
	//the first parameter is left blank so a new user is created
	$account = user_save('', $fields);	//Tell user about account and give them recovery link
	$tokens = array(
		'@name'				=>	$name,
		'!profile_link'		=>	l('Profile', '/user/'.$account->uid.'/edit'),
		'@days'				=>	intval(variable_get('anon_entity_cookie_expire_time', 2592000) / 68400),
	);
	drupal_set_message(t('New account @name has been created to own this content, you are presently logged in as this user with a @days day cookie. If you wish make this permanent please alter the !profile_link with your details.',$tokens));
	return $account;
}

/**
 * Function to authenticate and login a user given username and password
 * @param $username String The username of the account to be authenticated
 * @param $password String The password of the account to be authenticated
 * @return NULL
 *
function anon_entity_user_authenticate($uid)	{
	global $user;
	$user = user_load($uid);    
	//login finalize
	watchdog('anon_entity', 'Session opened for %name.', array('%name' => $user->name));
	$user->login = REQUEST_TIME;
	db_update('users')
		->fields(array('login' => $user->login))
		->condition('uid', $user->uid)
		->execute();
	drupal_session_regenerate();
}

// */

/**
 * Function to authenticate and login a user given username and password
 * @param $username String The username of the account to be authenticated
 * @param $password String The password of the account to be authenticated
 * @return NULL
 */
function anon_entity_account_login($account)	{
	global $user;
	$user = $account;    
	//login finalize
	watchdog('anon_entity', 'Session opened for %name.', array('%name' => $user->name));
	$user->login = REQUEST_TIME;
	db_update('users')
		->fields(array('login' => $user->login))
		->condition('uid', $user->uid)
		->execute();
	drupal_session_regenerate();
}

/**
 * Recovery session cookie with token
 *
function anon_entity_recovery($token)	{
  if(!$token) return;
  //Get uid from token
  $record = anon_entity_get_record($token, 'recovery_id');
  //no record do nothing
  if(!$record) return;
  //no user in record do nothing
  if(!$record->uid)	return;
  $account = user_load($record->uid);
  if(!$account) return;
  //set cookie
  anon_entity_set_cookie($record);
  anon_entity_account_login($account);
}

// */


/**
 * Return uid of sid
 *
function anon_entity_set_uid($sid, $uid) {
  $num = db_update('anon_entity')
    ->fields(array('uid' => $uid))
    ->condition('sid', $sid)
    ->execute();
  return $num;
}
// */

/**
 * Get uid for sid
 *
function anon_entity_get_uid($sid) {
  return db_query("SELECT uid FROM {anon_entity} WHERE sid = :sid", array(':sid' => $sid))->fetchField();
}
// */

/**
 * Get sid for recovery_id
 *
function anon_entity_get_recover_sid($token) {
  return db_query("SELECT sid FROM {anon_entity} WHERE recovery_id = :recovery_id", array(':recovery_id' => $token))->fetchField();
}
// */

/**
 * Get recovery_id for sid
 *
function anon_entity_get_recovery_id($sid) {
  return db_query("SELECT recovery_id FROM {anon_entity} WHERE sid = :sid", array(':sid' => $sid))->fetchField();
}
// */

/**
 * Get record for key value pair default to sid
 */
function anon_entity_get_record($value, $key = 'sid') {
	$result = db_select('anon_entity', 'a')
	  ->fields('a')
	  ->condition($key, $value)
	  ->execute();
	$record = $result->fetchObject();
	return $record;
}

/**
 * Update timestamp on record
 *
function anon_entity_update_record_timestamp($record)
{
	static $record_set = FALSE;
	if($record_set) return;
    // Update the session timeout.
	$record->timestamp = REQUEST_TIME;
	drupal_write_record('anon_entity', $record, 'sid');
	$record_set = TRUE;
}
// */

/**
 * @section
 * This section to manage hooking of entities for anonymous users
 */

 
/**
 * @section
 * This section forked from the Session API module to provide a quick interface for storing
 * information in the session.
 */


/**
 * Create an empty string cookie. This is useful for fooling the
 * anon_entity_available() function when using Pressflow, which does not set
 * a cookie for anonymous users.
 */
function anon_entity_start_session() {
  $_SESSION['anon_entity_session'] = '';
  drupal_session_start();
}


/**
 * Additional hooks to trigger third party modules
 */

/**
 * Implements hook_user_presave() when email_confirm module present to add empty to filled
 * option for conformation.
 */
function anon_entity_user_presave(&$edit, $account, $category) {
  //don't add this if the module not installed.
  if(!module_exists('email_confirm')) return;
  //Changed !empty($account->mail) to empty($account->mail) here to capture this state
  if (!empty($edit['mail']) && (empty($account->mail) && $account->mail != $edit['mail']) && !user_access('administer users') && !drupal_installation_attempted() && (!isset($edit['email_confirmed']) || $edit['email_confirmed'] === FALSE)) {
    // Set a temporary session variable to indicate that the email was changed. Is used in hook_exit
    // to clear out a Drupal message set by the user module after user_save() is called.
    $_SESSION['email_changed'] = TRUE;
    email_confirm_build_mail($edit, $account);
    $edit['data']['email_confirm']['pending_email'] = $edit['mail'];
    $edit['data']['email_confirm']['expiration_time'] = (REQUEST_TIME + 86400);
    module_invoke_all('email_confirm', 'email change', $account->uid, $account->mail, $edit['mail']);
    if (module_exists('rules')) {
      rules_invoke_event('email_confirm_email_change_request', $account, $account->mail, $edit['mail']);
    }
    unset($edit['mail']);
    unset($edit['email_confirmed']);
  }

  if ((isset($edit['email_confirmed']) && $edit['email_confirmed'] === TRUE) || (!empty($edit['data']) && isset($edit['data']['email_confirm']) && $edit['data']['email_confirm']['expiration_time'] < REQUEST_TIME)) {
    unset($edit['data']['email_confirm']);
  }
}


