<?php


/**
 * @file
 * Session API admin functions.
 */

/**
 * Session API admin settings form.
 */
function anon_entity_settings_form() {
  // Find modules that implement hook_anon_entity_cleanup().
  $modules = module_implements('anon_entity_cleanup');

  // Cookie expiry.
  $form['anon_entity_cookie_expire_time'] = array(
    '#type' => 'textfield',
    '#title' => t('Cookie expire time'),
    '#description' => t("This module sets an additional cookie in the end users' browsers to represent the pseudononymous account set up for them. This value is the amount of time, in seconds, that the cookie will stay valid in a user's browser."),
    '#default_value' => variable_get('anon_entity_cookie_expire_time', 2592000),
  );
  // Entity Selections
  // Get the list of entity:bundle options
  $options = array();
  foreach (entity_get_info() as $entity_type => $entity_info) {
    foreach (array_keys($entity_info['bundles']) as $bundle) {
        $options[$entity_type . ':' . $bundle] =
          $entity_info['label'] . ': ' . $entity_info['bundles'][$bundle]['label'];
    }
  }

  $form['anon_entity_entity_selection'] = array(
    '#type' => 'checkboxes',
    //'#required' => TRUE,
    '#title' => t('Active Entities'),
    '#options' => $options,
    '#default_value' => variable_get('anon_entity_entity_selection', array()),
    '#description' => t("Check all entity types that you wish to be enabled for this. Don't forget to give Anonymous users create permissions for the entity."),
  );

  return system_settings_form($form);
}
